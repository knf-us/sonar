#ifndef Sonar_h
#define Sonar_h

#include "Arduino.h"

class Sonar
{
  public:
    Sonar(int TP, int EP);
    void Timing();
    long Ranging();

    private:
    int Trig_pin;
    int Echo_pin;
    long duration, distance;
    
};

#endif
