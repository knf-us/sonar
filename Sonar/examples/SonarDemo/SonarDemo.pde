#include "Sonar.h"
#include <LiquidCrystal.h>
LiquidCrystal lcd(11, 10, 9, 4, 5, 6, 7);
Sonar sonar(12,13);

void setup() {
lcd.begin(16, 2);
lcd.print("Testing...");
}

void loop()
{
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print(sonar.Ranging());
  lcd.print("cm");
    
  delay(100);
}




